module.exports = {
  theme: {
    fontFamily: {
      display: ['Poppins', '"Century Gothic"', '"ITC Avant Garde Gothic"', '"TeX Gyre Adventor"', '"Helvetica Neue"', 'Helvetica', 'Arial', 'sans-serif'],
      body: ['"HK Grotesk"', 'sans-serif'],
    }
  }
}
