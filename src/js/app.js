import feather from 'feather-icons';

const scrollMonitor = require("scrollmonitor");

import Swal from 'sweetalert2';

import smoothscroll from 'smoothscroll-polyfill';

// kick off the polyfill!
smoothscroll.polyfill();


let menuButtons = document.getElementsByClassName('menu-button');

for (let b of menuButtons) {
    let menu = b.parentElement.parentElement.getElementsByClassName('menu')[0];
    menu.classList.add("closed");
    let toggleState = 0;
    b.addEventListener("click", e => {
        if (menu.classList.contains("closed")) {
            menu.classList.remove("closed");
        } else {
            menu.classList.add("closed");
        }
        toggleState += 1;
        toggleState %= 2;
    });
}

var menu = document.querySelector('nav');
menu.classList.add('initial');
var watcherEl = document.querySelector('.menuwatch');
if (watcherEl === null) {
    watcherEl = document.createElement('span');
    watcherEl.classList.add('menuwatch');
    document.body.prepend(watcherEl);
}
var watcher = scrollMonitor.create(watcherEl);
watcher.fullyEnterViewport(function () {
    menu.classList.add('initial');
});
watcher.exitViewport(function () {
    menu.classList.remove('initial');
});

for (let el of document.getElementsByClassName('fade-up')) {
    var fadeWatcher = scrollMonitor.create(el);
    fadeWatcher.enterViewport(function () {
        el.classList.add('animated');
    });
}

let l = document.getElementsByClassName('shorten');

/**
 * 
 * @param {string} text - the text to split into words
 * @param {string?} start - start index of words to split; if end is not provided, start is assumed to be the end
 * @param {string?} end - the end index of words to split
 */
function words(text, start, end) {
    var re = /[^\s]+/g; // words are whitespace-separated
    var i = 0;
    var wordsArray = []; // list of re.exec matches
    var match;
    while ((match = re.exec(text)) !== null) {
        wordsArray.push(match);
    }
    if (start !== undefined) {
        if (end === undefined) {
            var end = start;
            start = 0;
        }
        wordsArray = wordsArray.slice(start, end);
        var startIndex = wordsArray[0].index;
        var endIndex = wordsArray[wordsArray.length - 1].index;
        console.log(wordsArray, startIndex, endIndex);
        return text.slice(startIndex, endIndex);
    }
    return wordsArray;
}

let length = l.length
for (let i = 0; i < length; i++) {
    let el = l[i];
    el.innerHTML = el.innerHTML.trim();
    let more, summary;
    // Split by words, but if lines are more than three, split by them instead.
    const nwords = el.innerHTML.split(/\s+/).length;
    console.log(nwords)
    if (nwords > 40) {
        summary = words(el.innerHTML, 34);
        console.log(more);
        // more = el.innerHTML.substring(120, el.innerHTML.length);
        more = words(el.innerHTML, 35, nwords);
        // summary = el.innerHTML.substring(0, 120);
        let lines = el.innerHTML.split('<br>').map(a => a.trim());
        if (lines.length > 3) {
            lines.splice(3, 0, '');
            more = lines.slice(3, lines.length).join('<br>');
            summary = lines.slice(0, 3).join('<br>');
        }
    }
    if (more) {
        let $more = document.createElement('span');
        $more.classList.add('more');
        $more.innerHTML = more;
        $more.style.display = "none";
        let $morebutton = document.createElement('a');
        $morebutton.innerHTML = " [more]";
        $morebutton.onclick = (($more, $morebutton) => function () {
            $more.style.display = "inline";
            $morebutton.style.display = "none";
        })($more, $morebutton);
        el.innerHTML = summary;
        el.appendChild($more);
        el.appendChild($morebutton);
    }
}

function popup(text) {
    if (typeof text === "object") {
        if (typeof text.html !== "undefined") {
            text.html = text.html
                .replace(
                    /([+]*[(]?[0-9]{1,4}[)]?[\t -\d]{4,}[\d])/g,
                    '<a href="tel:$1">$1</a>'
                )
                .replace(
                    /((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))/g,
                    '<a href="mailto:$1">$1</a>'
                );
        }
        Swal.fire(text);
        return;
    }
    Swal.fire('', text, '');
}

window.popup = popup;

// let navAppearElements = document.getElementsByClassName('nav-appear-after');
// let shouldHideNav = navAppearElements.length > 0;

// if (shouldHideNav) {
//     let stickyNav = document.querySelector("nav.sticky")
//     stickyNav.style.display = "none";

//     for (let el of navAppearElements) {
//         let watcher = scrollMonitor.create( el );
//         el.style.marginTop = window.innerHeight * 3 / 4 + "px";
//         el.style.position = "absolute";
//         watcher.enterViewport(function() {
//             console.log("FIEREN");
//             stickyNav.style.display = "";
//         });

//         watcher.exitViewport(function() {
//             console.log("FIEREN");
//             if (watcher.isAboveViewport) return;
//             stickyNav.style.display = "none";
//         });
//     }
// }

for (let el of document.querySelectorAll('h1[id],h2[id],h3[id],h4[id],h5[id],h6[id],div[id]')) {
    let anchor = document.createElement('a');
    anchor.classList.add('anchor');
    anchor.id = el.id;
    el.id = "";
    el.parentElement.insertBefore(anchor, el);
}

// function anchorLinkHandler(e) {
//     var targetID = this.href.slice(this.href.indexOf("#"));
//     var element = document.querySelector(targetID);
//     if (!window.IntersectionObserver || !element.scrollIntoView) {
//         element.focus();
//         return;
//     }
//     e.preventDefault();

//     const originalTop = element.getBoundingClientRect().top;
//     const originalLeft = element.getBoundingClientRect().left;
//     const intersectionObserver = new IntersectionObserver(entries => {
//         let [entry] = entries;
//         if (entry.isIntersecting) {
//             setTimeout(() => {
//                 element.tabIndex = "-1";
//                 element.focus();
//                 window.location = this.href;
//             }, 200);
//         }
//     });
//     // start observing
//     intersectionObserver.observe(element);
//     // element.scrollIntoView({
//     //     behavior: "smooth",
//     //     block: "start",
//     //     inline: "nearest"
//     // });
//     window.scrollBy({
//         top: originalTop,
//         left: originalLeft,
//         behavior: "smooth"
//     });
// }

// const linksToAnchors = document.querySelectorAll('a[href^="#"]');

// for (var i = 0; i < linksToAnchors.length; i++) linksToAnchors[i].onclick = anchorLinkHandler;

if (window.netlifyIdentity) {
    window.netlifyIdentity.on("init", user => {
        if (!user) {
            window.netlifyIdentity.on("login", () => {
                document.location.href = "/admin/";
            });
        }
    });
}

feather.replace();
