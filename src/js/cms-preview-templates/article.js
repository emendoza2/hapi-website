import React from "react";
import format from "date-fns/format";

export default class PostPreview extends React.Component {
  constructor(props) {
      super(props)
      this.state = {
          image: []
      };
  }

  componentDidMount() {
    const {entry, getAsset} = this.props;

    getAsset(entry.getIn(["data", "image"]))
      .then(image => {this.loadImage(image)});
  }

  loadImage(image) {
    this.setState({image: image.url});
  }

  render() {
    const {entry, widgetFor} = this.props;

    let dateString = entry.getIn(["data", "date"], "");

    if (Date.parse(dateString) !== Date.parse(dateString)) {
      dateString = Date.now();
    }

    let date = format(dateString, "MMM d, y h:mm a");

    let author = entry.getIn(["data", "author"]);

    console.log(this.state.image);

    return <div className="container mx-auto py-5">
      <h1>{ entry.getIn(["data", "title"])}</h1>
      <div className="flex justify-between text-grey-500">
        <p><em>Published { date }</em></p>
        { author && <p>By { author }</p> }
      </div>
      <div className="cms mw6">
        <p>{ entry.getIn(["data", "description"]) }</p>
        {/* { this.state.image && <img src={ this.state.image } alt={ entry.getIn(["data", "title"])} /> } */}
        { widgetFor("body") }
      </div>
    </div>;
  }
}
