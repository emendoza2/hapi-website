import React from "react";
import CMS from "netlify-cms-app";

// Import main site styles as a string to inject into the CMS preview pane
import styles from "!css-loader!postcss-loader!sass-loader!../css/main.css";

import ArticlePreview from "./cms-preview-templates/article";

CMS.registerPreviewStyle(styles, { raw: true });
CMS.registerPreviewTemplate("articles", ArticlePreview);
CMS.init();