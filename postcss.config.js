class TailwindExtractor {
  static extract(content) {
    // eslint-disable-next-line no-useless-escape
    return content.match(/[A-z0-9-:\/]+/g) || [];
  }
}

const plugins = {
  "postcss-import": {},
  "postcss-preset-env": {
    browsers: "last 2 versions"
  },
  tailwindcss: {},
  autoprefixer: {}
};

if (process.env.NODE_ENV === 'production') {
  plugins['@fullhuman/postcss-purgecss'] = {
    content: ['./site/**/*.html', './site/**/*.md', './src/**/*.js'],
    extractors: [
      {
        extractor: TailwindExtractor,
        extensions: ['html', 'css', 'js'],
      },
    ],
    whitelist: ["md:inline"], //yuck. I don't know why purgecss won't recognize this!
    fontFace: true
  };
}

module.exports = {
  plugins
};
