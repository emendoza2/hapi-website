# HAPI Website
[![Netlify Status](https://api.netlify.com/api/v1/badges/4d26e45e-2cac-44b6-a18b-9582446fb80c/deploy-status)](https://app.netlify.com/sites/hapihomeschoolph/deploys)

All the files for the Hugo site for HAPI.
Hosted by [Netlify](https://netlify.com).

## Prerequisites

Clone this repo

```sh
yarn install
```

```sh
brew install hugo
```