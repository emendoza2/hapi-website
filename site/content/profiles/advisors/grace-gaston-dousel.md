---
title: Grace Gaston-Dousel
image: advisors/grace-gaston-dousel.png
categories:
- Advisors
---

- Homeschool mom to 2 for 8 years
- Educator
- Home education consultant
<!--more-->
- Learning coach 
- Entrepreneur
- Published author 
- Artist
- Co-founder of Arrows & Quivers Family Development, Inc.