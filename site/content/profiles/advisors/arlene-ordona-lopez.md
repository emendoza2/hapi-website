---
title: Arlene Ordoña-Lopez
image: advisors/arlene-ordona-lopez.png
categories:
- Advisors
---

- Homeschooling mom of 1 for 4 years
- Homeschool advocate from Cavite Homeschool Support
- Co-admin of two homeschooling Facebook groups
<!--more-->
- Former College faculty 
- Online thesis adviser 