---
title: Jade Mariñas
image: advisors/jade-marinas.png
categories:
- Advisors
---

- Independent, eclectic, Catholic homeschool mom of 5
- Founder of Facebook group: Dipolog Homeschool Network