---
title: Ainjele Miranda
image: execom/ainjele-miranda.png
categories:
- Execom
---

- Homeschool Nanay of 2 boys for 5 years and counting
- Program Director of Akda 
- Girl Scout of the Philippines Troop Leader 
<!--more-->
- Admin of Homeschool Resources and Support PH