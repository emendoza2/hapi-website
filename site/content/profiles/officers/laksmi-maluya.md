---
title: Laksmi Maluya
image: officers/laksmi-maluya.png
position: President
order: 1
categories:
- Officers
---

Laksmi Maluya is the current president of HAPI. She is an educator, and is herself a product of homeschooling from elementary to high school.
<!--more-->
She's been homeschooling three for 19 years.
In addition, she is also the founder of Gopala Learning Haven, a homeschool support and homeschool community provider.