---
title: "Officers"
---

We are a diverse group of core volunteers committed to making the nation a better place through homeschooling. Our leaders each have an important role to play in the organization. 