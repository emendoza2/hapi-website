---
title: Beday Fortuna
image: officers/beday-fortuna.png
position: Vice President
order: 2
categories:
- Officers
---
- Homeschool Mom of 1
- HR and Administration Head, Catholic Filipino Academy
- Directress, CFA Math-Inic
<!--more-->
- Founder and Executive Directress, Math and Science Camps, Philippines
- Consultant, ISO 9001/QS 9000/Malcolm Baldrige
- Quality Award & Systems Accreditation Consultant, DepEd Accreditatio