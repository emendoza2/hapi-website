---
title: Edric Mendoza
image: officers/edric-mendoza.png
position: Founder
order: 5
categories:
- Officers
---

- Founder of Homeschool Association of the Philippine Islands
- Homeschool dad of six for 14 years
- Founder of Parentschooling
<!--more-->
- Host of HomeschoolTV