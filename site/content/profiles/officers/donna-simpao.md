---
title: Donna Simpao
image: officers/donna-simpao.png
position: Corp. Sec.
order: 4
categories:
- Officers
---

Donna is the current HAPI Corporate Secretary. She is the co-author of Kistarter Curious Curriculum. Furthermore, she has been a homeschool mom for 16 years, <!--more--> and is the founder of Homeschoolers of the Philippines with over 30,000 members.

She is on the board of directors of the Visions of Hope Foundation. 
You can view her amazing blog at [HomesCool.ph](http://homescool.ph).