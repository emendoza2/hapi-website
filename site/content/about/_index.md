---
title: About
---
<div class="float-right pl-5"><img class="w-32 pb-5 block md:inline" src="/deped.svg"><img class="w-32 block md:inline" src="/hslda.png"></div>

Since we started in 2010, our goal has been to support homeschoolers and equip leaders to shape and grow the
national homeschooling movement. We're run by a diverse core group of volunteers and are affiliated with
[HSLDA (the Homeschool Legal Defense of America)](https://hslda.org), the largest homeschooling
organization in the world. We're
also partnered with the Philippines's [Department of Education](https://www.deped.gov.ph/).