---
title: "Homeschool Leaders General Assembly"
link: https://hapi.ph/articles/2020-when-together-we-can-do-more/
date: 2020-01-18T15:30:00.000Z
enddate: 2020-01-18T20:00:00.000Z
---

Equipping and supporting homeschool parent-teachers and advocates in serving the homeschool community.