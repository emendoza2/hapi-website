---
title: 'HOMESCHOOL HOUR: WORLDWIDE NATIONAL HOMESCHOOL DAY CELEBRATION'
author: Maria Lota Matias
date: 2021-03-05T10:15:00.000Z
tags:
  - Homeschooler Hour
description: ''
images:
  - img/Homeschooler Hour-Worldwide.png
---
### **HAPPY NATIONAL HOMESCHOOL DAY!**

We heard it quite often since we started the celebration of the National Homeschool Day from **March 1 to March 5, 2021**. The day of the National Homeschool Day is actually celebrated yearly, every 3rd of March. We remember the journey we had as Filipino parent-teachers, guardians, teachers and students, we had become a community of homeschoolers throughout the Philippines and some parts of the World.

Below is an article shared to us by **Pio Gerard Fulache Antiquina**, from **Seton Home Study School**, headed by **Ms. Cai Acuña**. He shares to us his own experiences about the HAPI Homeschool Hour and the performances of fellow homeschoolers who had taken the time to share their talents during that day he had co-hosted the event.

*Let me take this opportunity to also thank Pio and his co-hosts' efforts! Thank you, Seton Home Study School, Ms. Cai, for hosting the HAPI Homeschooler Hour Worldwide! Plus all the amazing performances made. You all did a great job!*

<br>

![homeschool hour - worldwide](img/Homeschooler Hour-Worldwide.png)

**<br>**

> **March 5, 2021.** It was a wonderful sunny Friday and I was hurriedly preparing for the HAPI Homeschool Hour. My co-host Marina Espinoza (Mari) messaged me over 4-5 times already but I was unable to respond quickly because I was still rehearsing my lines. With my polo on, hair styled with hair gel, I was ready to give my best! Oh, and I was only wearing shorts since only my upper body will be seen. I quickly turned on the laptop, accessed my Zoom, and joined the meeting.
>
> **If I had one word to describe our preparation, it would be the word “awesome.”** I never thought that our Seton representative, Ms. Cai Acuña, would make a script for us in advance. I assumed that we would have to make one ourselves from scratch but I thought wrong. Discussing, brainstorming, and joking with online friends whom I’ve known for quite some time was such a memorable experience. We then practiced our lines over and over again until we got the tone, pronunciation, and facial expressions right. Moreover, Ms. Cai pointed out our mistakes and helped me and my co-hosts to improve. At the end of our last meeting, the day before the event, Ms. Cai gave us words of encouragement and through her energetic charisma, she fueled enthusiasm into our hearts. 
>
> **With a few tips and motivating words, the HAPI Hour was now broadcasting all over the world.** Ms. Cai and Mrs. Laksmi Maluya started the event with opening remarks and were followed by introductions of the hosts. Mari introduced herself first, Faustine de Los Reyes, JD Caparas, then I introduced myself, followed by the Montas, Yedda and Lian. Various performances were displayed such as singing, dancing, reciting a poem, and playing the guitar.* **My favorite was Damodar das Castillo’s performance** when he played a classical piece with the cello. I play the violin so I appreciated his rendition since he was the only classical player in the event.* As I was hosting, I made some minor mistakes but Faustine said there were none so I guess I was alright. Fast forward to the end, my co-hosts and I closed the event with a prayer said by the Living Saints Homeschool Support and Tutorial center and said our farewells. Afterward, we had our last meeting as a team in which Ms. Cai affirmed us of our wonderful hosting talents and congratulated us on our performance. I wished we were together physically so that we could’ve eaten together as a team and celebrate our success, unfortunately, that was not the case. 
>
> **In conclusion, my HAPI hour experience was fun, educational, and full of amazing performances.** Even though I’ve only met my co-hosts through the Internet, we bonded as if we’ve known each other for a lifetime. Most of us did not have much experience in hosting but after the event, we realized that we were capable of such a role. On the educational part, enunciation, and facial expressions were essential in producing a world-class execution on hosting. I could tell that each homeschooler gave their best in each video, whether it was by baking, sports, music, literature, etc., they all gave their all for the greater glory of God. Despite this pandemic, which has hindered all kinds of events, HAPI has managed to bring together Filipino homeschoolers to share their God-given talents with the world. ***May the Good Lord watch over us and keep us safe. Soon in the future, we may all see each other face to face!***
