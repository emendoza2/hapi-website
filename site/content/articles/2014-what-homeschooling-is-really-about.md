---
title: What Homeschooling is Really About
author: Joy Mendoza
date: 2014-11-01T06:38:44.053Z
tags:
  - Homeschooling
  - Getting Started
categories:
  - Blog
original article: https://teachwithjoy.com/2014/11/homeschooling/
---
I talk a lot about homeschooling, but I want you to know that my children aren’t always cooperative, there are days when I don’t feel like teaching, and sometimes I am the less than perfect mother who gets annoyed with her kids.

Two days ago, I was teaching Titus from his Singapore Math book and he couldn’t get subtraction using number bonds. I could tell he was guessing so I elevated my pitch and my tone was agitated. As I explained to him the concept of regrouping by 10, subtracting the ones from each other, and adding what was left, he was confused. I probably did a bad job of communicating this process and I expected it to click in his head right away. Well, it didn’t. I gripped the pencil he was holding and circled and scratched on his book for emphasis as I went over each problem.

Titus began to tear. I thought,*Why can’t he get it?! Is there something wrong with him?! It’s not complicated!*

Well, there was something wrong with me. I was making homeschooling about me. What I wanted…my desired outcome…my teaching…my time…my effort…my way…my disappointment…OH, MY!

When I saw him struggling to stay composed, I felt horrible. Immediately, I apologized to him and hugged him, asking for his forgiveness. “Will you forgive me for being irritated? Mommy was wrong.” He readily accepted my apology and we pressed on. By the end of the session he figured out how to approach his math problems with confidence.

As for me, I was reminded that I am prone to reactiveness and impatience when my heart is in the wrong place. The key is to remember***why***I am homeschooling, to keep sight of the goal, which is to teach my children to love God with all their heart, soul, mind, and strength.

While teaching a subject like mathematics may be important, this is really a minute aspect of the real objective. Edric and I share a daily responsibility to nurture, encourage, and meet the needs of our children to grow in wisdom, stature, favor with God and favor with man. (Luke 2:52) Therefore, our homeschooling isn’t about 4 hours of the day when they are seated in our study room for lessons. It’s a lifestyle that ministers to our children’s spiritual, social, mental, and physical persons.



**SPIRITUAL**

***Homeschooling is discipleship.***While academics have a place, the greater emphasis is teaching our children to have a personal relationship with Jesus, love God’s word, submit to authority, and develop Christ-like character.

When our second son was little, he was nicknamed the “chairman” for being a very serious and grumpy boy who would often say*no*. Edric and I talked about his attitude and realized we had failed to be intentional about sharing the gospel to him. A few weeks after Edric did so, our son was a transformed child. His heart became malleable and teachable. He would even tell me, “Jesus is my best friend.” More importantly, he became a kinder, more considerate boy.

Today, Edan initiates reaching out to other children, organizing activities and games for them, and he is also assistant teacher to my younger kids. While he still has character issues from time to time, I can see the fruit of God’s work in his life.

Discipleship is the key to homeschooling. It’s impossible to teach a child who doesn’t want to listen. When my children don’t have the right attitudes there’s no point in proceeding with lesson time unless I address their attitudes first. Otherwise, it will be a battle of the wills between my children and me.

There have been instances when I have asked my older children to excuse themselves from our study room so they can have a moment to prayerfully consider their heart issues. While I don’t believe in asking little kids to stand in a corner for “time outs,” I do believe in asking older children who have a relationship with Christ to take the time to think through their feelings and actions in light of God’s Word.

*Are they acting and behaving in a way that pleases God? How can they change and improve if they aren’t?*

I prefer to proceed when they are spiritually ready, when they have returned to me after the Holy Spirit has ministered to them. Almost always, he convicts them about the wrongfulness of their responses to the task at hand, to me, or to others. It is amazing how a moment of purposeful reflection leads them to God-honoring conclusions. (Of course I also pray that they will be attentive to what God has to say to them during that period of pause.)



**SOCIAL**

Parents’ apprehensions about homeschooling often center around the socialization question. “What about their socialization?” I’d like to quote Elijah, my eldest. Once upon a time, a friend suggested he should go to school so he could have friends. His spontaneous relply: “I have so many friends, I can’t even count them!” He wasn’t exaggerating. Like my other kids, they aren’t friend-starved.

**While we don’t focus on*making*friends, we do focus on*how to be*a friend**. The emphasis is on social development — training our children to look beyond their insecurities and comfort zones so they can be a blessing and channel of Christ’s love. Furthremore, in the context of family, there are numerous opportunities to practice relationship principles like unconditional love, forgiveness, humility, or “do unto others what you would have them do unto you.” In fact, the family is often the hardest place to apply these principles! As much as we all love one another in our family, there are days when we don’t*like*each other. The challenge is to transcend this feeling by availing of the grace that Christ supplies.

**Social development happens most naturally at home.**Between a husband and wife, siblings, parent and child, each member of a family must die daily to selfishness and self-centeredness. They must choose to love, forgive, make sincere apologies, and grow in their understanding of one another. A child who can relate to others in this manner will not be in want of good company.

Furthermore, a child who has received love, appreciation, who is accepted for who he or she is, and allowed to fail and make mistakes will be inspired to learn. I remember an instance when Titus came to me in fear. His face was half-visible behind the sliding glass door that separated the room from the bathroom.

“Mom I did something.”

“What is it?” I asked. He was hesitant to confess his deed at first, but then I prodded him to do so.

“I hit the shuttlecock into our neighbor’s yard.”

*That’s it?!*I thought.*Why couldn’t he tell me that right away?!*

“It’s okay. I forgive you. It was an accident.” I said reassuringly.

“Why were you afraid to tell me that?

“I thought you would be mad.”

“Do I get mad a lot?” (I had to check.)

“No.”

“Well, I want you to know something. I love you no matter what and I will always forgive you.” I repeated it again until I was sure he internalized this.

He flashed a big smile and then ran off to play again.

I may not lose my temper with my kids and yell at them, but I do get irritated from time to time. So I have to be careful and mindful of the way I relate to them. I need to ask myself this question:*Am I cultivating a relational climate that gives my children the liberty to express their heartfelt longings, fears, ideas, or confess their mistakes?*The relationship I have with my kids impacts my ability to instruct their hearts and their minds. If they can trust me with who they are, they can trust me to teach them who they should become.

**MENTAL**

What is our schedule like when it comes to lessons?

**Monday, Tuesday, Thursday, Friday**

7:00 – Bible Reading (as a family)

7:30 – Breakfast

8:30/9:00 – **Lessons**

12:30/1:00 – Lunch

2:00 – Nap/Play/Exercise

6:00 – Dinner

8:30 – Bedtime

[![IMG_8928](https://i1.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8928-1024x768.jpg?resize=640%2C480)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8928.jpg)

On Wednesdays, we get together with other homeschool families. A good number of ladies in my discipleship group are homeschooling their kids and they have women in their groups who are also homeschooling. Wednesdays is the day we have designated to hold classes so our kids can interact and work with other kids. I’m so blessed by the moms in this group who lend their expertise and creativity to teach art, music, bible, character, science, etc. We also asked an awesome physical trainer to teach our kids sports and fitness.

[![IMG_8350](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8350-e1414973572142-768x1024.jpg?resize=640%2C853)](https://i0.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8350-e1414973572142.jpg)

[![IMG_8344](https://i1.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8344-1024x768.jpg?resize=640%2C480)](https://i0.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8344.jpg)

[![IMG_8007](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8007-e1414973803768-768x1024.jpg?resize=640%2C853)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8007-e1414973803768.jpg)

[![IMG_8008 1](https://i1.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8008-1-1024x768.jpg?resize=640%2C480)](https://i0.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8008-1.jpg)

When my kids and I are at home, our lessons happen around a large table. I assign tasks to my children and act the part of a facilitator. Elijah and Edan can do a lot of work on their own. Titus and Tiana need more attention from me. Catalina is “exiled” so we can focus. She is entertained by our househelp. (Praise God for househelp!)

[![IMG_8599](https://i0.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8599-1024x1024.jpg?resize=640%2C640)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8599.jpg)

[![IMG_8610](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8610-e1414974031344-768x1024.jpg?resize=640%2C853)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8610-e1414974031344.jpg)

[![IMG_8611](https://i0.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8611-e1414975428851-768x1024.jpg?resize=640%2C853)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8611-e1414975428851.jpg)

[![IMG_8613](https://i1.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8613-e1414975562819-768x1024.jpg?resize=640%2C853)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8613-e1414975562819.jpg)

Ideally, it would be nice if all my kids sat around the table and stayed put, but I’m a pretty laid-back homeschooling mom. They can do some work on the floor or on the couch. They can even migrate to different rooms if this will help them accomplish their tasks. Sometimes, we even homeschool in the car if I absolutely have to do an errand in the morning!

My philosophy when it comes to teaching is simple:***a child needs to master the essentials so he will become a self-directed learner.***I am more particular about skills like math, reading, comprehension, logic and reasoning, rather than science, history, Filipino, social studies, etc. If my kids are confident with the essentials, they will have the building blocks to learn whatever they want to. I don’t want them to be held back by me. As much as possible, I try not to hover around them all the time. In fact, I tell them, “you can figure it out.” (Sometimes I have to say this because I don’t know how to explain it either!)

Unless they are really stumped, I encourage my kids to rise up to the challenge of a difficult task. This is one of the reasons why my boys are turning out to be good at math even if I’m terrible at it! I also encourage them to study what they are interested in, beyond what we are covering during their lessons. Since I don’t canabilize the day with instruction, they have a lot of free hours to pursue topics that are meaningful to them. Instead of burdening myself with the responsibility of teaching them EVERYTHING, I zone in on the basics and point them in the right direction by giving them access to a multitudinous number of books, and supplementing their learning with educational apps and internet sites that are pre-approved.

For example, some months ago my older sons memorized the periodic table of elements, just for fun. It wasn’t part of their science requirements to do so, but they were fascinated by it. So I let them use an app (Toca Lab) that helped them to understand all the elements and their abbreviations. When they weren’t using the app, they would play a game where they named all the elements and gave the symbols to match them. I don’t even know the periodic table of elements! I kept getting the symbol for Iron wrong when they would “quiz” me! It’s*Se*right?!

The point is I am very aware that I have cognitive limitations as their teacher so I don’t pressure myself to be the expert. If they want to learn about a topic that I’m not familiar with, I find out what resources I can connect my children with or to so they can become the experts.



**PHYSICAL**

The physical aspect of homeschooling has to do with developing our children’s talents, inspiring productive hobbies, giving them lots of play time to explore, build, create, and making sure they get adequate exercise and rest. Our children are enjoying a “relaxed” childhood. They don’t have to rush off to school, spend hours in traffic, or come home exhausted only to do more work.



**CHECKLIST**

We evaluate our children’s progress and growth by asking these questions:

IS MY CHILD…

* *Living a transformed life because of his/her relationship with Jesus Christ?*
* *Developing a love for God’s Word?*
* *Rooted in God’s Word?*
* *Submitting to my authority with an attitude of respect?*
* *Growing in Christ-like character?*
* *Secure in my love for him/her?*
* *Loving others, especially his/her siblings?*
* *Thinking of others as more important than his/her self?*
* *Mastering essential skills that will enable him/her to reason and defend his/her faith, and effectively communicate the gospel truth?*
* *Developing his/her talents?*
* *Playing and enjoying his/her childhood?*
* *Pursuing productive interests and hobbies?*
* *Getting enough exercise and rest?*

Edric and I keep these questions in mind as we homeschool our kids so we know if we are pointing them in the right direction. When we sense that they are off-course, we re-evaluate and re-calibrate so we can correct where they are headed. We also look at our own lives and examine if we are exemplifying the values and principles we want them to internalize.

Like I said earlier, it’s not a perfect lifestyle. It can be challenging and tiring to keep training and teaching our children. It can be discouraging when we fail as parents. However, I am constantly amazed at the daily grace God provides to keep us going.

I remember an instance when I was stressed about homeschooling, and my older son, Elijah, commented, “You know John Wesley’s mother, Susanna Wesley, had 19 children.”*In other words…mom, if she could do it then so can you. You’ve got it pretty easy with just five!*More importantly, Susanna Wesley was a woman of faith and spiritual excellence. If I want to raise children who will love the Lord with all their heart, soul, mind and strength, I have to love God with all that I am first. That’s the secret to successful homeschooling.

[![IMG_8282](https://i1.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8282-682x1024.jpg?resize=640%2C960)](https://i2.wp.com/teachwithjoy.com/wp-content/uploads/2014/11/IMG_8282.jpg)

READ ABOUT SUSANNA WESLEY HERE: http://susanpellowe.com/susanna-wesley.htm
