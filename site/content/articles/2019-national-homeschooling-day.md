---
title: "The Philippines’ National Homeschool Day"
date: 2019-01-24
images:
  - /img/HDN2019.jpg
author: Donna Simpao
description: >-
    In the words of Senator "Kiko" Pangilinan, “We believe that if we are to shape our nation, we will have to shape first our communities, and if we are to shape our communities, we have to shape our families.” In January 2017, HAPI core member then, Dr. Donna Pangilinan-Simpao suggested to reach out to the Senate of the Philippines.  The idea of setting a day as the National Homeschool Day for your country was raised.
categories: 
    - News
---

Back in 2016, HAPI core members brainstormed on projects to create homeschooling awareness in our country. Yes, HAPI was already at forefront of homeschool conferences then.  The idea of setting a day as the National Homeschool Day for your country was raised.  HAPI checked and even submitted the day 3rd of March to an online site that sets national days for various causes but that route didn’t pan out.  

Around the same time, a  group of homeschoolers led by Nove Tan of the Philippine Homeschool Association set also an audience with the Senator Francis Pangilinan who was a believer of home education and was part of the Senate Committee on Education 2002-2004. Homeschooled children were also part of the meeting and they were even interviewed during this dialogue. 

Nove Tan recalls this meeting with great fondness. Nove shares what Senator Kiko Pangilinan said, “We believe that if we are to shape our nation, we will have to shape first our communities, and if we are to shape our communities, we have to shape our families.”  She also adds that  Senator  Kiko recognizes the need to craft a law for homeschooling as the movement grows and that we need to institutionalize this mode of education that ensures that homeschoolers are able to have the quality of education our kids deserve. 

In January 2017, HAPI core member then, Dr. Donna Pangilinan-Simpao suggested to reach out to the Senate of the Philippines through her brother, Senator Francis “Kiko” Pangilinan.  HAPI was then informed that indeed many groups have requested for resolutions for a national day celebrating their advocacies. The office of Senator Kiko Pangilinan then showed HAPI samples like the National Day of Youth in Climate Action set on November 25 and National Day for Healing for Unity and Peace set on March 6.  The office of Senator Pangilinan through their Deputy Chief of Staff Rachel Gilego coordinated with HAPI as there were supporting documents needed.

On March 3, 2017, the 17th Congress of the Republic of the Philippines,  the Resolution Expressing the Full Support of the Senate of the Philippines in Celebration of the First National Homeschool Day was passed by the 17th Congress of the Republic of the Philippines;  [Senate P.S.R No. 208](/NHD2017.pdf) Hallelujah!!!!!!

In this resolution, homeschooling was recognized as an educational process available to Filipino parents and was in keeping with the right and responsibility of every Filipino parent to raise their children.

HAPI, Homeschool Association of the Philippine Islands, was also recognized in this resolution as on organization that aims to equip homeschooling groups and organizations in growing the national homeschooling movement. 

And the rest is history.  Come the 3rd of March 2019, we shall be celebrating our 3rd National Homeschool Day with a theme “ #HAPItogether, Sama-Sama, Tulong-Tulong”.

There are many ways to celebrate this year’s National Homeschool Day. Gather your students, and your support groups or co-ops. Rally your homeschool provider.  Think of creative ways to celebrate the values of  unity, cooperate and support/help for one another as homeschoolers…. “Indeed #HAPItogether, Sama-Sama, Tulong-Tulong”.


Here are some suggestions: 

1.	Have a garage sale of homeschool materials! Share a portion of your income to give to others in need. 
2.	Do a cleanup drive. Together with your homeschool support group, visit a park or a street and just clean up!
3.	Visit a homeschooler who may benefit from some help at home and chores. 
4.	Set aside books, toys, and other educational materials for donation to a home for orphans or to a public school library. 
5.	Do a joint fundraiser and use the income to support a shared advocacy. 
6.	Gather together with other homeschoolers for simple fun and games! Let the kids  share their talents through a program.


Document your activities and post your photos/ videos on your different social media platforms with the hashtag #hapitogether #nationalhomeschoolday2019! You may also share them on Homeschoolers of the Philippines FB Community Group. 

Happy 3rd National Homeschool Day to All! 

*Thank you to Nove Tan @[ihomeschoolph](https://ihomeschool.ph/) for sharing her memories regarding the first ever National Homeschool Day*
