---
title: TEEN HOP's Homeschooler Hour
author: Maria Lota Matias
date: 2021-05-24T00:11:46.957Z
tags:
  - Homeschooler Hour
images:
  - img/teenhoppic.jpg
---
### **BRAVE TO FACE CHALLENGES**

Our experiences teach us the value of courage, determination and strength to move forward. We encounter different opportunities each day but it is up to us if we will back down or face them head on. 

I remember every time I was asked to sing in front of a huge crowd. I always feel nervous that I might forget the lines of the song but also excited to perform my heart out. But whenever I start singing and see the audience my heart starts pumping and I felt my courage building up.  Then, after that I hear the applause from the crowd, which fills my heart with joy.

I think these same feelings were felt by **TEEN HOP's hosts**: Andie Yusay (Grade 12), Ethan Lopez (Grade 10), and Sarmiento siblings, Bea (Grade 11) and Zito (Grade 6), for ***Homeschooler Hour***, as part of the *National Homeschool Day celebrations*.

<br>

**Read their story below, from TEEN HOP Special:**

![](img/teenhoppic.jpg)

<br>

> It’s the recently concluded five day ***National Homeschool Day celebration last March 1-5, 2021*** initiated by the Homeschool Association of the Philippine Islands (HAPI) with the theme ***“HAPI for Juan and All.”*** For the first time, many homeschoolers are given a platform to showcase their talents and serve the whole homeschooling community.
>
> On **March 3***, the **Teen Homeschoolers of the Philippines - Teen HOP*** - was exclusively given two full hours to highlight outstanding teens in different fields in sports, visual arts, leadership and communications, and performing arts during the Teen of the Hour portion. The Homeschooler Hour Teen HOP Special also placed a spotlight on a variety of presentations, ranging from solos, duets, Hiphop and interpretative dances, piano rendition, band performances, and fun games!
>
> *\*Homeschooler Hour Teen HOP Special, National Homeschool Day Celebration, March 3, 2021 via Zoom.*
>
> *\*Teen Homeschoolers of the Philippines (Teen HOP) is a homeschooling support group exclusively for teens aged 10 years old and above.*

<br>

![](img/teenhoppic1.jpg)

**Event Hosts: (left to right) Zito, Bea, Andie, and (lower center) Ethan.**

<br>

#### *“The NHD celebration reminded me that I am blessed to be homeschooled!” - ANDIE YUSAY, GRADE 12*

<br>

### *ANDIE*

> The Homeschooler Hour Teen HOP Special was hosted by four homeschooler members, Andie Yusay, Ethan Lopez, and siblings Bea and Zito Sarmiento. We feature all four hosts to give us a look behind the scenes and tell us their personal experiences and takeaways during this event. 
>
> “When I heard that **Likha Public Speaking Club*** was invited to host the Homeschooler Hour and they need volunteers for the NHD event celebration, I immediately told myself to step out of my comfort zone.
>
> It is daunting to realize that you can make mistakes in front of a wide audience in a zoom casted show but I dared to be brave and applied my learnings in Likha Public Speaking Club. The dry run of the event allowed me to know more of my strengths, as well as my shortcomings, and gave me the opportunity to do what I can to improve during the actual show. It became an enjoyable experience for me because it showed me the many talents of my fellow homeschoolers and their insights on their own homeschooling journey. This event reminded me that, as a homeschooler of five years, I am blessed to be homeschooled!" ***\- Andie Yusay, Grade 12, Homeschool Global - Victory Christian International School***
>
> *\*Likha Gavel Club is a public speaking club exclusively for Teen HOP members accredited by Toastmasters International.*

<br>

![](img/teenhoppic2.jpg)

**Our Homeschooler Hour event hosts with guests during a Zoom dry run**

<br>

### *ETHAN*

> “I ran into many difficulties while co-hosting this online event. Although it is very organized and I'm no stranger to live streaming, interviewing a person through Zoom was definitely a difficult task. It wasn't easy to form that 'bond' with my co-hosts and the guests through a computer screen, which in turn, made it harder for me to keep the spirit of the conversation lively with my jokes.
>
> Albeit a challenge, co-hosting the National Homeschool Day's interview segments with Andie, it was an exciting opportunity for me to enhance my speaking and interpersonal skills! It's a first, and I hope it isn't my last. It was fun being able to meet so many new people, and working alongside such determined personalities was very enjoyable! I'm happy to have been able to take part in this year's celebration of the National Homeschool Day! It was a fulfilling experience, to be able to give back to the community that has helped shape my homeschool journey of seven years.” ***\- Ethan Lopez, Grade 10, Peniel Integrated Christian Academy***

<br>

### *BEA & ZITO*

> “My brother Zito and I are grateful for the opportunity to be hosting together as a duo for the first time. We didn’t hesitate to volunteer and immediately began to prepare and practice our scripts and give our best to serve. We were able to gain confidence from our training in Likha Gavel Club, especially when there were technical difficulties on the day of the event which required spontaneous and quick thinking. We are happy to have been able to contribute to the National Homeschool Day in our own little way.” ***\- Sarmiento siblings Bea, Grade 11, and Zito, Grade 6, Homeschool Global - Victory Christian International School***

<br>

> NHD 2021 is for homeschoolers by homeschoolers! This is the first time the teens themselves hosted a national celebration, specifically for the homeschooling community. Instead of the day being an official holiday and enjoying a rest day, they found themselves at work, meeting other homeschoolers from all over the country and beyond, sharing ideas, and HAPI-lly serving together.

**Congratulations TEEN HOP, Ms. Leny Yusay for supporting all the teens in this event of the homeschool community!** You all did a great job, Andie, Ethan, Bea and Zito, as well as all the teens that performed in our National Homeschool Day celebration, you made us all believe in courage and the strength that's within our hearts!
