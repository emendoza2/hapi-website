---
title: Updated List of HAPI Members
author: Laksmi Maluya
date: 2021-05-12T23:57:00.000Z
tags:
  - HAPI Members
images:
  - img/all logos.jpg
original article: 'https://www.facebook.com/TeacherPurpleLaksmi/posts/829126497696564'
---
*\[An excerpt from my facebook post.]*

**Homeschool Association of the Philippine Islands** (HAPI) is an association of homeschool providers/support groups/schools who offers homeschooling.

**Members are as of 2016.**

Every year we renew membership.

**HOMESCHOOL Providers, Support service providers and support groups.**

\> Catholic Filipino Academy (CFA)

\> Homeschool Global (TMA)

\> Homeschool Asia Pacific (HAP)

\> International British Academy (IBA)

\> Kids World Integrated Academy

\> Peniel Integrated Christian Academy of Rizal Inc.

\> Philippine Christian School of Tomorrow

\> Victory Christian International School

\> Kairos

\> The Learning Basket

\> FB Homeschoolers of the Philippines

\> Rockers

\> Gopala Learning Haven (Gopala Play Center)

Each of our member runs on their own.

As volunteer core group members, we collaborate/exchange best practices/work with Deped/do projects and the like to advocate and promote homeschooling as a viable and respectable choice of families in educating our children.

I, as one of the Core Group Members/Program consultant, personal desire and now a mission thru HAPI is to assist/gather the "indie" not to put you in place but to inform/share/organize so that we can represent your voice better.

I know where you come from because I am a product of "indie" homeschooling from preschool to high school. I am the product of what you now choose to do with your children. Did I do well? Let's meet and talk! ![🙂](https://www.facebook.com/images/emoji.php/v9/t4c/1/16/1f642.png)

So please help me help you get heard. We cannot do this alone. The homeschooling movement is growing and we want to spread "Homeschooling for all the right reasons."

This has been and my advocacy for more than 30years now but it is only this year that I joined the bigger circle. I have been in my small circle all these years.

Have a blessed day!

<br>

**Edited to add 2020 Members. (Providers and Support Service Proivders & Support Groups)**

![](img/all logos.jpg)

**Members since 2016**

\> Catholic Filipino Academy (CFA)

\> Homeschool Global (VCIS)

\> Homeschool Asia Pacific (HAP)

\> International British Academy (IBA)

\> Kids World Integrated Academy

\> Peniel Integrated Christian Academy of Rizal Inc.

\> Philippine Homeschool Association (PHA)

\> FB Homeschoolers of the Philippines

\> Gopala Learning Haven (Gopala Play Center)

<br>

<br>

**New Members**

\> Arrows & Quivers

\> Better Together Homeschool

\> Living Learning Homeschool

\> The Classical Mentors

\> TMB Madrasah-Montessori Learning Center

\> Seton Home Study School, Philippine

\> Akda Philippines

\> Living Pupil Homeschool

\> Homeschoolers of Cebu

\> Ihop (Indie support group)

\> Teen HOP

\> Cavite Homeschool Support

\> Dipolog Homeschool Network

<br>
