---
title: 'Homeschooling in the Philippines: Where to Get Support'
author: Tina Rodriguez
date: 2014-01-27T07:21:44.413Z
tags:
  - Homeschooling
  - Support
categories:
  - Blog
description: >-
  Most homeschoolers would probably agree with me when I say that homeschooling
  is less stressful and challenging when you know that you’re not alone. Support
  is important and, as we all know, “no man is an island,” and I think this
  applies especially to homeschooling parents (who are mostly moms).
original article: >-
  https://trulyrichandblessed.home.blog/2014/02/27/homeschooling-in-the-philippines-where-to-get-support/
---
***This post is the fourth of 7 posts that I'll be writing this week, as [my response to Jen @ Conversion Diary’s 2nd Epic Blogging Challenge](http://trulyrichandblessed.home.blog/7-homeschooling-posts-in-7-days-my-response-to-the-conversion-diary-challenge/). To God be all the glory!* ![:)](https://i0.wp.com/trulyrichandblessed.home.blog/wp-includes/images/smilies/icon_smile.gif)**

If you’re a homeschooler or thinking of becoming one, you need not fear! There are many support groups for homeschoolers nowadays compared to before, according to more “veteran” Filipino homeschoolers. Even if these are mostly online, support is still support, right? 😊

Before I list down the homeschool support groups that I’m aware of, allow me to say that if you are enrolled with a local provider like[Catholic Filipino Academy](http://catholicfilipinoacademy.com/) (CFA), you will find it easier to get support. CFA, in particular, has parent coaches *and* parent coordinators per grade level, to help you in your homeschooling journey. They also have Facebook communities (groups) for certain groups, like the K1-K2 parents, Grade 2 parents, high school students, etc. The CFA South parents and students, in particular, are quite active and meet regularly. (CFA South meaning they are mostly located in the Southern part of Metro Manila, and the nearby provinces.)

Another way to get support is through homeschool co-ops, like those organized by[The Master’s Academy](http://www.tmahomeschool.org/)(TMA). Other homeschoolers — independent and otherwise — form their own co-ops.

Having said that, here’s a list of homeschool support groups (mostly online) that you may want to join:

**Pinoy Homeschool Yahoogroup (pinoyhomeschool@yahoogroups.com)**— An email group composed of different homeschoolers, ranging from those with DepEd-accredited providers and those who are independent and unschooling (Sorry, the link to the group description page on YahooGroups seems to be missing/dead!) There is a Facebook group with the same name though, which you can find [here](https://www.facebook.com/groups/292091590922974/?fref=ts). Here’s the group description:

> This is the FB page for members of PINOYHOMESCHOOL yahoogroup (formed in 2001)…FB’s features would allow the members to connect better…from YG homepage we find the ff description of the group: This forum will help us explore and process homeschooling ideas and materials, ensuring that what we use in our home are relevant and responsive to the Filipino culture and to our children’s unique needs and learning styles…This group is not affiliated to any homeschool programs in the Philippines, although some of our members/members’ children may be involved/enrolled with any of them.

**[Proverbs 22:6ers Yahoogroup](https://groups.yahoo.com/neo/groups/proverbs22-6ers/info)—**An email group composed mostly of Christian homeschoolers. Below is the group description:

> Proverbs 22:6ers is a Filipino homeschool community who believes that it is the responsibility of the parents to “train up a child in the way that he should go,” just as the Bible commands it.

**[Homeschoolers in the Philippines](https://www.facebook.com/groups/303988516361928/?fref=ts)**— A Facebook support group for Filipino homeschoolers

**[Cebu Homeschool Network](http://cebuhomeschoolnetwork.blogspot.com/p/about.html)**— A blogsite to help homeschoolers in Cebu. Below is the description:

> Cebu Homeschool Network is your starting place for exploring homeschooling here in Cebu.\
> The group was started by Ms. Chelo Echaves, Maria Martha Acla, Fleire Castro and Joan Po who met at the Pinoy Homeschooling Yahoo Group and decided to meet and discuss our homeschooling experiences every month.\
> Along the course of out meetups, we have also met up with other families who were not yet into homeschooling but are very interested in this option for educating their kids.\
> This blog will contain our experiences and journey with homeschooling, our activities and events that provide support to all of us homeschoolers.\
> We welcome every homeschool association, support group and homeschooling families into our network.

**[ROCKERS Philippines](https://www.facebook.com/groups/276301965729544/) —**This is the community that I believe God led me to form, after I met [Liza Castaneda](http://www.smartparenting.com.ph/community/sp-mompreneurs/mompreneurs/dance-your-way-to-success-liza-dela-fuente-casta-eda-of-ridgepointe-ballet), another Catholic independent homeschooler. ROCKERS stands for “**Ro**man **C**atholics **K**eeping **E**ducation **R**eal.” We interact mostly online, but try to meet at least once every quarter (it used to be once a month but people have been busy!). The group is made up of current and former Catholic homeschoolers, and still-discerning-about-homeschooling Catholics in and outside the Philippines. We do have a few non-Catholics in the group though — everyone is welcome! 😀 One of our “trademark” events is our yearly All Saints Day party — you can see a few photos of last year’s party [here](https://www.facebook.com/media/set/?set=a.548873438522936.1073741880.216714075072209&type=3).

Of course, there is also the [Homeschool Association of the Philippines Islands](http://hapihomeschooler.com/), or HAPI for short. Here’s the description of HAPI from our website:

> Homeschooling has been around in the Philippines for probably since the 70’s through American Christian missionaries who came here with their families. But it has only been in the last 10 years that a surge of awareness and interest has been observed. Momentum has picked up steadily, with more homeschool providers rising up to support the families that desire to do it as a choice over conventional schooling.
>
> [![](https://hapihomeschooler.files.wordpress.com/2012/02/hapi_logo_hi-res14.jpg?w=240&h=178 "HAPI_logo_hi-res[1]")](http://hapihomeschooler.files.wordpress.com/2012/02/hapi_logo_hi-res14.jpg)
>
> HAPI stands for “Homeschool Association of the Philippine Islands”, and is a non-stock, non-profit advocacy of Filipino homeschoolers. Incorporated in June, 2009, it then mobilized leaders of various homeschool groups to mount projects and initiatives to help Filipino homeschoolers-at-large. In October 2010, HAPI was formally launched during the 3rd Philippine Homeschool Conference, and has since gathered approximately 3,000 member parents and children.
>
> HAPI which is run by a diverse core group of volunteers is affiliated with HSLDA (Homeschool Legal Defense of America), the largest homeschooling organization in the world.
>
> **HAPI’s MISSION:**
>
> To be a catalyst in nation-building, by advancing home education and unifying and empowering home educators across the Philippines.
>
> **HAPI’s VISION:**
>
> HAPI envisions a movement of transformed families that are compassionate, authentic, competent thus creating positive change in their communities and the nation as a whole.

\*\*\*\**

As I mentioned before, I’m a member of the HAPI Core Group but have been mostly inactive due to my current ‘season in life’ — being the mom of three young kids. However, I do hope to be able to join our next meeting, which is very soon! 😀

Now, let me ask you: Are **you** a homeschooler? Where do you get YOUR support? I’d love to know!Let’s talk in the comments! 😀

\*\**Coming up next: A special **Freebie Fridays** post featuring **free Catholic homeschool curriculum***

*\*\*Don’t miss the first three posts in my 7 in 7 series (plus a BONUS one!):*

[Homeschooling in the Philippines — Answers to some FAQs](http://trulyrichandblessed.home.blog/homeschooling-in-the-philippines-answers-to-some-faqs/)

[The Week and Day That Was: A Peek at Our Homeschool Days](http://trulyrichandblessed.home.blog/the-week-and-day-that-was/)

[Independent Homeschoolers in the Philippines](http://trulyrichandblessed.home.blog/independent-homeschoolers-in-the-philippines/)

[Word-Filled Wednesdays: Encouraging Bible Verses for Homeschoolers](http://trulyrichandblessed.home.blog/word-filled-wednesdays-encouraging-bible-verses-for-homeschoolers/)
