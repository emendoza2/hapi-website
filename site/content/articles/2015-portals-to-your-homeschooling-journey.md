---
title: Portals to Your Homeschooling Journey
author: Donna Simpao
images:
  - /img/undraw_navigator_a479.svg
date: 2015-12-11T08:02:44.751Z
tags:
  - Homeschooling
categories:
  - Blog
description: 'We''ve put together a list of articles new homeschoolers may find useful. '
---

## Why Homeschool?

[Top 5 Reasons for Homeschooling](http://trulyrichmom.com/my-top-5-reasons-for-homeschooling/) by Tina S. Rodriguez

## ​Homeschooling Basics/ FAQs

[Homeschooling Now Part of the Philippine Educational Framework](http://www.hapi.ph/hapi-do-021-2019.html) by Dr. Donna Pangilinan-Simpao\
[\
Answers to Common Questions about Homeschooling](http://www.fullyhousewifed.com/2014/10/04/answers-to-common-questions-people-ask-about-home-schooling/) by May De Jesus-Palacpac\
​\
[Homeschooling in the Philippines: FAQs and Tips for Parents](https://www.smartparenting.com.ph/parenting/preschooler/homeschooling-in-the-philippines-faqs-and-tips-for-parents) (SmartParenting.com)\
\
[Homeschool Blogs in the Philippines](http://www.mommyjourney.com/2016/03/homeschool-help-homeschool-blogs-in-the-philippines.html) by Chris Amador\
\
[First Time Homeschooling Parents Guide](https://blog.nationalbookstore.com/first-time-homeschooling-parents-guide-homeschooling-philippines/)(National Bookstore)

## First Time Homeschooling

[Ten Things Every Homeschooling Family Should Know](http://www.fullyhousewifed.com/2012/08/08/10-things-every-homeschooling-family-should-know/) by May De Jesus-Palacpac\
\
[Ten Things You Need to Know Before Homeschooling Your Kids](https://ph.theasianparent.com/homeschool-101-10-things-you-need-to-know-before-homeschooling-your-kids/) (The Asian Parent)\
\
[What Homeschooling is Really About](https://teachwithjoy.com/2014/11/homeschooling/)by Joy Tan-chi Mendoza\
​\
[Dear First Time Homeschooler](http://homescool.ph/2016/05/14/dear-first-time-homeschooler-series-help-for-those-who-have-taken-the-plunge/) by Donna Pangilinan-Simpao\
\
[Homeschooling in the Philippines: FAQs and Tips for Parents](https://www.smartparenting.com.ph/parenting/preschooler/homeschooling-in-the-philippines-faqs-and-tips-for-parents)(Smart Parenting)

## Costs of homeschooling

[How Much Does it Cost to Homeschool Your Child](https://www.smartparenting.com.ph/parenting/preschooler/how-much-does-it-cost-to-homeschool-your-child-a618-20170909-lfrm) (Smart Parenting)\
​\
[How to Homeschool Your Kids Without Losing Your Mind](https://www.smartparenting.com.ph/parenting/big-kids/how-to-homeschool-your-kids-without-losing-your-mind-a324-20180903) (Smart Parenting)

## Homeschooling and Working

[Recalling and Savoring My Blessings as a Homeschool Mom While Earning](https://www.handsonparentwhileearning.com/recalling-and-savoring-my-blessings-as-a-homeschool-mom-while-earning/) by Teresa Gumap-as Dumadag

## Homeschool Providers

[\
Where to Enroll Your Child Locally](http://trulyrichmom.com/homeschooling-in-the-philippines-where-to-enroll-your-child-locally/)by Tina S. Rodriguez\
\
[Homeschool Provider Fees](http://www.filipinohomeschooler.com/homeschool-provider-fees) by Que Gavan\
\
[DepEd-Accredited Homeschool Providers in the Philippines](http://thehomeschoolingwahm.com/deped-accredited-homeschool-providers-in-the-philippines) by Daise Virtudazo\
\
[Accredited Homeschool Providers](http://www.mommyjourney.com/2015/06/homeschool-help-accredited-homeschool-providers.html) by Chris Amador\
\
[Homeschool Provider: Peniel Integrated Christian Academy of Rizal Inc.](https://www.petitemomma.com/2017/06/homeschool-provider-peniel-integrated.html)by Kat Santiago\
\
[Homeschooling posts](https://entirelyofpossibility.wordpress.com/category/homeschooling) by Joie Villarama\
​

## Support for Homeschoolers

[Homeschooling in the Philippines: Where to Get Support](http://trulyrichmom.com/homeschooling-in-the-philippines-where-to-get-support/) by Tina S. Rodriguez



## Best Practices in Homeschooling

[Our Homeschooling Setup for 2016-2017](http://mommyplannerista.com/2016/06/our-homeschooling-set-up-for-2016-2017/)by Janice Lim

## A Day in the Life of a Homeschooler

[The Week and Day That Was](http://trulyrichmom.com/the-week-and-day-that-was/) by Tina S. Rodriguez

## Independent Homeschooling

[Independent Homeschoolers in the Philippines](http://trulyrichmom.com/independent-homeschoolers-in-the-philippines/) by Tina S. Rodriguez\
\
[Independent Homeschooling Options for Filipino Homeschoolers](http://thehomeschoolingwahm.com/independent-homeschooling-options-for-filipino-homeschoolers) by Daise Virtudazo

## Encouragement​

[Homeschooling Journey Bad Days](https://elfamiliagarcia.wordpress.com/2018/07/14/homeschooling-journey-bad-days) by Familia Garcia

## Realities in Homeschooling

[Realities of Homeschooling](http://www.mommyjourney.com/2016/06/7-realities-of-homeschooling.html) by Chris Amador

## Homeschooling By Levels: Preschool

[Pre-K Homeschool](http://www.sheenalovessunsets.com/pre-k-homeschool/) by Sheena Gonzales\
\
[DIY Kindergarten Homeschool Curriculum](https://www.petitemomma.com/2015/05/diy-kindergarten-homeschool-curriculum.html) by Kat Santiago

## Homeschool Set Up: Classroom/ Organization

[Homeschooling Tips for Small Homes](https://www.petitemomma.com/2014/06/homeschooling-tips-for-homes-with-small.html) by Kat Santiago
