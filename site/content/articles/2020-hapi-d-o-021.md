---
title: HAPI D.O. 021
author: Donna Simpao
date: 2019-09-02T12:20:37.389Z
tags:
  - DepEd
categories:
  - News
description: >-
  Happy News from HAPI! Between 2014-2018, representatives of the Homeschool
  Association of the Philippine Islands (HAPI) and other proponents of the
  Alternative Delivery Mode (ADM) educational system worked together with the
  Philippine’s Department of Education through a series of workshops.
---
Happy News from HAPI!

Between 2014-2018, representatives of the Homeschool Association of the Philippine Islands (HAPI) and other proponents of the Alternative Delivery Mode (ADM) educational system worked together with the Philippine’s Department of Education through a series of workshops. The purpose of the exchanges was to define the scope of the Alternative Delivery Mode (ADM) system of the enhanced K-12 Basic Education Program. Due to the renewed K-12 goal to raise holistically developed Filipinos with21st Century Skills, the Department of Education resolved to understand the evolving educational landscape of our country by running a series of workshops.

The HAPI working committee was led by Edric Mendoza (Homeschool Global/The Master’s Academy) with committee members: Laksmi Maluya (Gopala Learning Haven/ Independent Homeschooling), Dr. Ingrid Yap (Kids World Integrated), Great Mabuti (Homeschool Global/The

Master’s Academy), Beday Fortuna (Catholic Family Academy), Micheal Chua (School of Tomorrow), and Sheina Dumulot ( HAPI-DepEd Liaison/Homeschool Global).

![](https://lh3.googleusercontent.com/jli1sY8_oSFsJPOj11fILyzUQw0LuasCS5gz9GylFdXjIMS2B4n2--e_xCjzUNoL1mmcKHiT6WX4tC8lTkJRfn2gRECXK9zxCIUaWCR7bBxq14AeJRVXAC6gQP8jSbrugF7TpU4W)

Proponents for the Alternative Delivery Mode (ADM) all in smiles !

![](https://lh3.googleusercontent.com/pLXbf5IWJ5d8J8o4jikEFM6ZkTEI5bFkqyugpqNN_G17-8kmDhLdJw1k-lJmxAx9cq1iGwdcI9_syo_VthH0oZE56N8Oi2-dOfLvq6OJ5mL_fglumCg38iY5lWuF8QIOVgnFR60o) HAPI Members: Laksmi Maluya, Beday Fortun, Michael Chua and Ingrid Yap present homeschooling to other ADM educators.

![](https://lh3.googleusercontent.com/5iwHVpQsmiVuE_U-m1ZsFJaHwVc37GUU8h13XbqHemeWcN1-EOXnKowQcWtfF-t3FUGPqvZLGkTMGKG2WN5NLVtpxBm5Y_Uc5gh9yOwuI1-j6e4TesV4PVnnp_OrNmCWNuc7npu7)

The “homeschool” table during the ADM workshop: (From L-R) Laksmi Maluya, Sheila Domulot, Beday Fortuna, Edric Mendoza, Michael Chua and Great Mabuti.

![](https://lh3.googleusercontent.com/bidiLKXstbur7mTfWu6Fg0KW5NMoHYHd7dpYcsi7Dt2aekZH5aYnDYmCaA1y_YxFiscLxn8YXg8yMIlgMi9vZ8wAh19OOGeSL5A2HGlaCcUALOz9LKHbqN1G16-DxF8HbZ_qz7kB)

Thank you HAPI!

The plan then was to include “homeschool” as an approach where the parent/parents or a responsible guardian takes the responsibility of educating their child/children at home.

Previously only a DepEd Memo (DECS Memo no. 216, 1997) recognized homeschooling in the Philippines under the title “Home Education Program”. It was introduced mainly to “provide an alternative delivery system of educating children who for some reason or circumstance could not attend formal school.”

The HAPI Working committee and Philippine Homeschool Association through Nove Tan were in coordination with DepEd’s Senior Education Program Specialist Ms. Angel Jabines regarding the outcome of the series of workshops and ultimately the recognition of “homeschooling” as one of the modalities in the new framework of the alternative delivery of education.

Mid 2017, HAPI received information from DepEd that homeschool was included in the “Flexible Learning Options (FLO)” as seen in this diagram.

![](https://lh4.googleusercontent.com/idRa6JJPRlM_z5O1OxZdKYWXIozJ6rVDvHUQWwTBFx1iBGB6gGxM3W5ULMzbhbd39FDowucXxvTUASb_wQ46ndj9xXkzQDa9T1vRMBqbgCuu4Q7Jire10OWhgnsUbEb6TWqIaj78)

Last August 29, 2019, members of HAPI received this message, “Homeschool is officially part of the DepEd program. DownloadD.O. (DepEd Order) 021 2019,” from Dr. Ingrid Yap, founder and owner of Kids World Integrated.

And on page 7 of this 153 page document, there it was: “homeschool”.

In a conceptual diagram showing the pathways and equivalences between formal educational system and alternative learning system, “homeschool”is identified as one of five ADM (Alternative Delivery Mode) modalities. Currently, the five modalities under the ADM are:

1. Homeschool
2. IMPACT (Instructional Management by Parents, Community and Teachers)
3. MIMOSA (Modified In-school and Off-school Approach)
4. Night School
5. Open High School

To learn more about these modalities, head to <https://www.deped.gov.ph/wp-content/uploads/2019/08/DO_s2019_021.pdf> and read Annex 3 on pages 96-100.

![](https://lh3.googleusercontent.com/8YfzVjsC7VkLUnxvFkf25PvLdPR8LWxQKN6d122Xpx1M5E9izHxibR9pmrOuO3KqPEdx4L9XF83PMrMxgxKb4SK1_CbG1njtccoZWgf8icuhf9iwf10eCHBNmalo5Yl0hFUCYfV4)

Source: [deped.gov.ph](https://www.deped.gov.ph)

The Department of Education (2019) defines home schooling in this manner:

“Homeschooling provides learners with access to formal education while staying in an out of school environment. Authorized parents, guardians and tutors take the place of teachers as learning facilitators. While learners are expected to meet the learning standards of the Basic K-12 Curriculum, the learning facilitators are given flexibility in delivery , the scheduling, the assessment and curation of resources. The program aims to cater to learners who may require homeschooling because of their unique circumstances, such as illness, frequent travelling, special needs education needs, and other similar contexts. Moreover, the program allows parents and guardians to maximise their involvement in their children's education as a matter of parenting philosophy. " ( Annex 3, Policy Guidelines on the K-12 Basic Education Program, Deped D.O. 021, August 22, 2019)

However, this department order instructs homeschoolers to home educate through the following available DepEd recognized entities:

1. Public schools
2. Private schools with permits to offer homeschool programs
3. Homeschool providers

Homeschoolers who opt to homeschool “independently” without any alignment or enrolment with any DepEd recognized facility that offers homeschool programs are not identified in this order.

Laksmi Maluya after assessing the outcome as laid out in the DepEd Order recently released, shares, “Homeschool being under the ADM program meant homeschooling is considered an alternative delivery mode of learning when traditional setting is not feasible. But it seems for the DepEd’s point of view traditional setting is still the 1ST choice. “

For HAPI that seeks to build a positive and supportive homeschool landscape in the Philippines, this is a breakthrough. HAPI President Donna Pangilina-Simpao, M.D. says, 

> Homeschooling is now recognized and included in “formal education system” framework. Many may appreciate the “inclusion” nature in other areas of education as seen in this 153++ page document but “homeschooling” as described and defined in this order is what our government recognizes for now. The working team (HAPI) tried their best to represent all homeschoolers, including those who would like to be “independent” and not enroll with any DepEd recognized or accredited center in our collaboration with our government. 

Homeschool is now officially part of the Philippines’ educational framework.
