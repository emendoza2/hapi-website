---
title: Dear First Time Homeschooler
author: Donna Simpao
date: 2016-05-14T06:42:43.336Z
tags:
  - Homeschooling
  - Getting Started
categories:
  - Blog
original article: http://homescool.ph/2016/05/14/dear-first-time-homeschooler-series-help-for-those-who-have-taken-the-plunge/
---
Dear First Time Homeschooler,

So you finally made the decision to homeschool? Let me congratulate you and yes, jump for JOY with you in making one of the best decisions you will ever make in raising your children. My children and I salute you ( See the photo???) Do not fret, many are with you. Many have gone ahead of you and many are willing to come alongside you. I have gone ahead of you for maybe several years and yes, because of several children, I am still homeschooling level 6 and 4 children. And I’d like to pass on what I have learned so far.

I am beginning this series so we can connect and so this may be used again and again for every first time homeschooler who is joining the homeschooling craze that is sweeping our country!

I co-moderate a Facebook Community Group called Homeschoolers of the Philippines (HOP). We started a little less than 2 years ago and from a handful of members, we have grown to 5300. I was able to verify our numbers when we we were able to fill up 1500 slots for a Kidzania Manila 3 day preview invitation 18 hours after the invitation was posted on the FB group . You are not alone.

There are many others, like you, defending their decision to homeschool to doubtful family members and friends. Stand firm and take courage.

Depending on the age and number of your children, you will have to face several concerns from hereon. And as my way of guiding you, let us take that several issues per blog post.

1. The first thing I’d like for you to do is to create a solid foundation for your homeschooling.

Create a Mission – Vision – Purpose for your homeschooling journey. Take time to draft this with your spouse/ partner and children. With toddler or too young preschool children, parents can brainstorm and put this together. You may [check this out](http://www.coach.net/vmpv.htm) for help in drafting your Vision (What you’d like to become) , Mission (What are the specific tasks/goals to achieve your Vision) and Purpose ( Why you exist? Why you do what you do?) . If the children are older, involve them as you set the back bone of your homeschooling. Older children can type this or create poster boards to draw these and display them in your study area. Some even make their own coat of arms to symbolize their collective Vision, Mission and Purpose .

Homeschooling is not an easy task but having a solid foundation of your vision, your goals and why you do what you do will always come in handy when the going gets tough. There will be bad days. There will be days when you can’t seem to decide what to prioritize. There will be days when the lesson plans you’ve set out are not working. Your solid Vision-Mission-Purpose can be a strong anchor; they can form a basis on which to leverage certain concerns and truly, they can help you choose the correct curricula/ books to use, activities to participate in and projects to do. In addition to that I have seen for myself how a solid foundation can truly save a terrible homeschooling day!

Another way to create a strong foundation is to get to know your self more and your spouse/partner in light of homeschooling. If you are a teacher by profession or if you have the gifts of teaching, then that is an added plus of course. However, if you are not , identify your ” teaching” strengths and weaknesses. Think of your own teachers in the past. What were striking among your favorite teachers? Why did you like these teachers? Think of what kind of teacher will bring best out of your own children and work towards being that kind of teacher. This will entail some research (online or interviewing other homeschoolers), maybe attend a few seminars, purchase a book on how to become a good teacher. Sometimes all it takes is asking your own students what they’d like their teacher to be! Style of teaching may eventually transform as our students grow up but you ought to try to be consistent in the basics of what makes a good and effective teacher. Assess as well your spouse’s/partner’s and other significant persons’ contribution or share in the task of home education. And while you’re at it, start praying for lots of patience, you’ll need a whole lot of this!

Many homeschoolers skip this part of forming a good foundation. Often, once a homeschooler decides to homeschool they pretty much dive into the curriculum hunt and purchase here and there. And many times, we end up with material that do not fit our Vision/Mission, personality, strengths and weaknesses and even learning styles of our student/s. This leads us to #2

2. Get to know your children in the eyes of a teacher. What kind of [learner](http://www.weirdunsocializedhomeschoolers.com/how-to-homeschool-determine-your-childs/) is your child? What are his passions? What excites him? What activities do you find him enjoying the most?

Take this simple quiz to find out more about your [child/children’s learning style](http://www.homeschool.com/articles/Ablaze5/default.asp).

3. Now, take time to read about the[ different homeschooling philosophies/teaching styles](http://www.homeschool.com/new/difstyles.asp). What form of homeschooling set up then will most like, fit or complement your current home life? Your personality? Your student’s personality and learning style? We have always been an eclectic homeschooler. Initially, for our first few years of homeschool, I was a bit more strict and academic heavy. We eventually relaxed our set-up to a more **relevant, hands-on, stories ridden, project based, application heavy approach** (haha, can you picture that? ) and hopefully that had lots of **problem solving and critical thinking challenges** for each student.

4. Based on your student’s/students ages, you will now have to decide whether you’d like to do things on your own and homeschool independently or if you’d like some guidance and have accreditation with Department of Education and enroll with a homeschool provider. This decision is important for Kinder levels up to Level 10 ( Level 11 and 12 homeschooling in the Philippines is not yet accredited by Department of Education ). The toddler/preschool period is actually the best time to try out homeschooling since you can do this without any need to have any accreditation or validation. This period provides more flexibility and allowance to do some form of trial and error, to learn and teach as you go without the pressure of dealing with tougher lessons or too many subjects, not to mention, the required grading!

Read here to get to know more about [independent homeschooling vs. homeschooling with a Deped accredited homeschool provider](https://thehomeschoolingwahm.wordpress.com/2015/02/20/independent-homeschooling-in-the-philippines/). Here is another blog post on [independent homeschooling in the Philippines](http://trulyrichandblessed.com/independent-homeschoolers-in-the-philippines/) by fellow homeschooler Tina Santiago-Rodriguez of [www.trulyrichandblessed.com.](http://trulyrichandblessed.com/) Learn more about the[ different ways Filipinos homeschool here.](http://myteachermommy.com/2015/01/31/the-homeschooling-alternative-homeschooling-in-the-philippines/)

So for now, I think, I have given you, my dear first time homeschooler enough stuff to think through and review. For our next post, we shall discuss on material/ curricula, subjects, and creating schedules .

Hope this first blog post in this series helps! Check out the different posts in this blog in various categories to get a sneak peek of our own homeschooling journey.

Sincerely,

Mommy Donna

If you need further encouragement regarding your decision, check this[ ONE](http://homescool.ph/2015/06/18/if-god-leads-he-enables-confessions-from-a-homescooler-of-13-years/) out. A post about seeing the fruits of your homeschooling labor :)



[![IMG_1669](https://i0.wp.com/homescool.ph/wp-content/uploads/2016/05/IMG_1669.jpg?resize=1024%2C970)](https://i0.wp.com/homescool.ph/wp-content/uploads/2016/05/IMG_1669.jpg)

> 2 Corinthians 4:3-4 *“Blessed be the God and Father of our Lord Jesus Christ, the Father of compassion and the God of all comfort, who comforts us in all our troubles so that we can comfort those in any trouble with the comfort we ourselves have received from God. “*
