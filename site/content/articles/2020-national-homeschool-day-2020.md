---
title: National Homeschool Day 2020
author: Janice Lim
date: 2020-02-20T08:23:00.000Z
tags:
  - Homeschool
  - Event
categories:
  - News
description: >-
  Who's ready to celebrate the 4th National Homeschool Day? We are!

  Given the directives of DepEd and DOH to limit gathering of many people in
  enclosed spaces, HAPI is encouraging homeschoolers to be creative in
  celebrating this day.
images:
  - img/4th-national-homeschool-day.png
---
Each year, we encourage homeschoolers to pay it forward as we celebrate. For this year, HAPI's theme is "Coming Alongside Each Other, Keeping the Bayanihan Spirit Alive" #SaloSaloTogether #NHD2020

![](img/4th-national-homeschool-day.png)

\
\
We are rallying up HAPI members to trek to Cavite on March 3, 2020, and support the clean up and fundraiser drive of Gopala Learning Haven c/o Laksmi Maluya. Gopala has been affected by the ash fall from Taal's eruption and thus has become non-operational for over 4 weeks now. We are “coming alongside” for Gopala Learning Haven and the Maluya family as we keep the Filipino *bayanihan* spirit alive.\
\
You're invited to join us too! There is NO REGISTRATION FEE, although we highly encourage a donation, in cash or in kind, to fund Gopala's rehabilitation efforts. Fill up the registration form below if you're sure to come and we will send you more information.\
\
<a href="http://tiny.cc/n8m5jz" class="btn">REGISTER HERE</a>

Are you planning your own event to celebrate NHD 2020? Let us know about it! Fill up [this form](http://tiny.cc/unh7jz) and tell us about your event.

![Win this book!](img/NHD2020 Giveaway 1.png "Why You Should Homeschool Raffle")

\
**Join this giveaway!**

<a href="http://tiny.cc/n8m5jz" class="btn inline-block">JOIN THE RAFFLE</a>

To kick off the celebration, HAPI is hosting a raffle over on our Facebook page. [Click here](http://tiny.cc/uze9jz) for a chance to win a copy of *Why You Should Homeschool* by Edric and Joy Mendoza.
