---
title: 'When Together, We Can Do More'
author: Donna Simpao
date: 2020-02-03T01:29:19.547Z
images:
  - /img/HAPIMembers.jpg
description: >-
  When homeschool movers and leaders come together, we can do more! Many years
  ago as a HAPI Member, I wanted to gather as many homeschool representatives or
  leaders in the hope that in coming together, we can come up with a collective
  summary of concerns that well represents homeschoolers in general.
categories: 
    - News
---
I believed then that HAPI, the Homeschool Association of the Philippine Islands, had to open its doors in order to be ”the group” that represents homeschoolers in the Philippines. Whether one homeschools independently or as enrolled with a provider, whether one comes from a Christian, Muslim or Catholic faith, or on opts for a secular approach, whether one is a Filipino based in the Philippines or is abroad, I was looking at HAPI to be able represent as many homeschoolers and to “mobilize and unite” movers and leaders so as to move forward in unifying homeschool concerns in our country.

![](https://lh5.googleusercontent.com/jqy2ozLD2RqLedjG-TMdWaII-DaqnmAa5Xc8a4ivV56F0Gcv1WWczRL0w6-Ju-v2IKW0_3hoAyKqrzpTRS2Q4B4ISRg62j3NY-RDBQbPZaiitgBvX_eMY8kQljzoaZyqso735_TC) Last January 18, 2020, this assembly finally happened. It was a special morning of overflowing energy, excitement and food!

![](img/IMG_5266.JPG)
With HAPI Members: Laksmi Maluya, Au Villegas and Edric Mendoza

![](https://lh5.googleusercontent.com/kPy3LDpS37HpzmW5KxbQaqBAD-q8tApZBRvNCh1agUDErPqNGnoar9mAy2nZ1BkchRLbZCTMj63Mq8XL8elabmdH6nyLXNQ8oM8iXdUCljwEn0lMgHy5HvYI4hAFeEE4aTXi6WO1) 

![](https://lh6.googleusercontent.com/-7UKsTQwRgUggXMvYkV0EccpP70FSAe_YV0K4v5FSgIeFLr9lHBFQ0NW2hj1NIO5fR5N6YidKxck8Z2CoSzAL6Fsch1NQMBGd107F9BML-0VG_9Y2-kEEhaXCAAiBNZq8Ts8jTPm)

With Homeschoolers of the Philippines (HOP) Moderators; Mico Damanlig Sadorra and Rachelle Clemente Martinez who together with Precious Santiago handled registration for the day; Mico was our program host.

![](https://lh4.googleusercontent.com/JXGej9P3hl3DxEw2Dtn2H6YMAe343Zz-A3AxQcEiNIG9dflQ2qPIcexK7EtfhGYxZ6p56rl5mMpDH6brimqxtOTntQZ1D7ulzTpyK90AMlDVtiSvf4z3pq9jhWoR9coguMuWf006) During this meeting, founding member and HAPI Chairman, Edric Mendoza, reiterated:

**HAPI’s mission:** 

to equip homeschool leaders who will shape the Philippine homeschooling landscape

**HAPI‘s vision:**

to see homeschooling recognized as available and respected form of education shaping leaders and movers who build our nation.

As HAPI recognizes and foresees the phenomenal growth of homeschooling in the Philippines, we, the current members, see the HAPI’s critical and relevant role in supporting and equipping various leaders that serve in the homeschool community. Such leaders may come from any sector of the homeschool community. Leaders may represent homeschool support and advocacy groups, homeschool providers and suppliers or the academe.

With the services offered by homeschool education providers, homeschool co-ops and support groups, and homeschool service suppliers, what sets HAPI apart is its role in leadership support and training, collaboration among leaders and partnership with the Department of Education and other agencies that may work with the homeschool community.

Edric further shared the plan of HAPI in achieving its vision:equipping homeschool leadersthrough regular meetings, training via seminars and conferences. He also reiterated that HAPI also needed the leaders in building towards achieving alandscape where homeschooling in the Philippines is readily available and is a respected form of education.

Being a non-profit organization that runs with volunteer members and their membership annual fees, there are limitations with which HAPI can execute its deliverables in achieving its goals----thus a renewed membership plan was presented and it is currently being rolled out.

Edric also showed a video about the upcoming Global Home Education Conference 2020--a gathering of Home Education leaders all over the world with Manila as house in November 2020.

Our attendees came from various sectors of the homeschool community. There were representatives from various homeschool support groups or co-ops, from after “homeschool” or tutorial services, homeschool speakers and coaches and homeschool providers.

As a way to encourage our attendees, I spoke on “10 Lessons Learned (and still learning) from Serving the Homeschool Community “. Since this day was set to also listen to our leaders, the group was divided into small groups to discuss some guide questions and each group presented on major homeschool concerns and suggested ways in which HAPI can help.

![](https://lh6.googleusercontent.com/xdntuDlhl_6Atq-87puSqz9fr0jhKXF4PvmGD57JuJr_poHlsnuzErC77Q7ny7qJziAuRGtanh-SZTqjMN5tqsqHs0fFjyfQAKXBLuaAYggOjG5iOqMQNNKpm8wNWJWFqAyJQrJD)

Homeschool movers and leaders were then divided into groups and were asked to reflect on what I had shared and brainstorm about : general homeschool concerns and suggested ways HAPI can help. Each group presented at the end of their discussion.

Indeed, we are stronger and can move farther when we are together, when we are willing to listen to each other and when we can focus on what unites us.

Here are more photos of this unforgetable HAPI day.

![](https://lh3.googleusercontent.com/G5Ord54pXoZJN2jesDrAkbypUJ1R6Ei07mCZlcDNtTTCNtr282Tjx0ynmBtkKVuk4Ku5rjK9pcksLFMBE1Oy-fnFwl9Se0XE1q_RXXwRhetjxfA0NmF0NTM2ldNFW4qpgOsnYcwk)![](https://lh4.googleusercontent.com/i_ZOLg6Iwhs000uj0jSjrUOvUuhqT_LhyQN0lnOZs2Qft-tZXH9F8PO6oIP24Q8wXcbGXDAG9wkCvDOiBhN_i4_N2lRIaAkouorg4ev1xA04v4_2w7NQCL1vlc5-U7yrsHVSc_55)![](https://lh5.googleusercontent.com/lXxvIYq765Qo149bm7riwbDmK2Ve4PixmMIV5FeEJ9emOqxja-_CZl59sri7LGpdBNCp5z_F_qD0PHHBeVP-o1LfJGFUS6GEil8eWiWvZhehVzW8zDKGfkagJ7SCbVXlY4S8Wb5O)![](https://lh3.googleusercontent.com/GDp84vamLCe2C68QsAFsKE-k-W3mtzSxVy0zRft-NHqc3lH5hAuu8eqJQs15pgYcPVqOjXoQO9l0JIhXO7Gu7PLA1s0pcLnevYt0kIRvRDJ9Wt-VYrMrGiWsTAz5ZvmWNLBezBRB)![](https://lh5.googleusercontent.com/U3vjBPZ3zKfzPbG9MDP6fE7we7pc5QoLx-5FYlGHqoILBC8TuqEX-zmZB-ZmGbpFmObQukZp1Wlat0gJMhJ4hjnJ2eHWGvk7h2Q1Ybcp95barRhKx1y_SCqqsqx77RSS43uw4MFa)![](https://lh6.googleusercontent.com/7jC3GXcB0fu4VEHcxFZYRyVkb-Pi9P03hI1JBPwb90Pazz4lALjI_9LFwG7JRwKUuVgFG6ihhKyHflaQ9rf0Mvk2zRL99c7lp8cKESUXba8bsTrM3dj-4h-Yof0w6KDnhxhReF0F)![](https://lh3.googleusercontent.com/opW6M2DUWyJ7L9wcklMl2GW8KGnEllfiv4yQ0twj-cii5RgLFlr2-J8GyyFHonA06dzK2HbworiFDytA6-JXOZ1uMcRU_P6iN-jBWhgq7akHkY_rCU9AjJ8GYiYirFXl0KIx64R-)![](https://lh6.googleusercontent.com/0-lhImz4ctTk3Vv2Ji87jWhBLu7rDiHksk42CLvjfwm0Wa4w97k5mIMUlSST9dzeSWba6AGW-oG743cwu47dx8vSNIXm0ERsdWNJ57sAmd6OIyf2Jl8T3DGV2cUz4KsNB-RCC5o7)![](https://lh5.googleusercontent.com/otwISNFC_kifAaNZKJVPZ2YyTG5qIDJIXAV-jFPdp5-aqiBWZHF5ideRCHwruHryagI-R1-JkMlzpGxfQy9k_WQ6EHMJc8_8IfVA4EREAQhTCuYYOEhf7tZDgKBHoJ_17A6-hsJt)![](https://lh5.googleusercontent.com/RA4EmhUmtpBbylul_myJ2PRQv-g5UFi5W6wOP0k677uP5dGRkOouCybNIECjxPZEZy4msxQ0h_BtB3pERJVyr5TIgNScpGHc5LEE--7Iq7LqB4U0bOpeVNzMUrJfDlTch2j2kc2w)![](https://lh3.googleusercontent.com/5zQj1DjWie7VIvxHlR9yJdKfSWuv3laJobP_5Cmr1MyEvC6XsXV4gaLKdunopgPUC0BVWj-c_ZaKa_gjofWdC2DgGTStUvpWjCAVZdmX7gF14yuwKXDPd2lf8i6oXEfcI0rIVqXu)

![](https://lh6.googleusercontent.com/7zXOgClSA01L9WhXK3DKrVsR4L6rdYW_BCjsvS7UPg3Z3m93LFOZqP3W2kbm6VY3-rc5i6XHDijgLJqTfL0IcXPR6h0GngHLaOBCoLyuu5jYzJFHyQyC6kuCyFtajCL4d8S3jcAK)

Indeed, the backdrop could not not be more “spot on” indeed. What a sobering reminder !![](https://lh6.googleusercontent.com/HZl7d9tf0K55WgvqzboTq9U3ZH0OTUmed0Pmhz17Yos6T_eob5FV80l-EPYHOaytZUGfo8VtPuaIV7skwrH-U96zH0X7OOQHhHe4qhVV_q0K0vfO57iRznVfwEHi7FRwfuQAE8yW)

If you would like to be a member of HAPI, click [here](/contact/).
