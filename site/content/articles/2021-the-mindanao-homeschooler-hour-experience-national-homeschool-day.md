---
title: 'The Mindanao Homeschooler Hour Experience: National Homeschool Day'
author: Maria Lota Matias
date: 2021-03-04T11:23:00.000Z
tags:
  - Homeschool Hour
images:
  - img/NHDPh2021 Mindanao.png
---
### **A WARM HEART FROM MINDANAO...**

In every celebration we had in our lives, there is always that moment when we feel the out pour of emotions all around us. Homeschooling teaches us that every day, life always gives us something we could celebrate for, like a step, a hop, a leap, a bound - these are the basic milestones in our lives we need to be thankful for.

I've been a distance learner in the past, when I was in my 4th year Highschool I did "blended learning", modules were given and we only come at school every Monday, Wednesday (academics) and Friday (P.E.). I finished my college degree just answering modules, listening to cassette tapes and researching on my own. Those were the 2 most important highlights of my life which I treasure now that I'm older, and even more because I became a homeschooling parent-teacher. 

This is also what **Laya Joaquin**, a homeschooler from **Arrows & Quivers Homeschool**, felt when she was handed a role as an emcee for the HAPI Homeschool Hour that features homeschoolers of Mindanao, as part of our National Homeschool Week's celebrations.

**Read her wonderful story below:**

<br>

![](img/NHDPh2021 Mindanao.png)

<br>

> **On March 4th, 2021, a few other homeschoolers from Arrows & Quivers and I hosted Mindanao’s Homeschooler Hour for this year’s National Homeschool Day.**
>
> **When I was offered to emcee, I didn’t even hesitate in accepting**. I’d emceed at a few events before, but none as big as this. It was honestly an exciting step for me. I could put all the skills I picked up from public speaking to good use. It did help that all the hosts already knew each other, and had an established chemistry, so there was hardly ever a silent moment in between all the video performances and games. 
>
> **I had been in charge of overseeing our Facebook live, because we wanted to include our viewers there in our games, too.** I saw a lot of comments praising the homeschoolers performing--not just on our live, but in the Zoom chat box as well. I looked through all of them, thinking about how heartwarming the words of encouragement were, and how it must’ve made the recipient feel. I know I would have melted from the compliments. 
>
> **It also thrilled me to see the individual works that all our homeschoolers showed us.** We all got to look deeper into the creative and educational lives of all these kids, in the way they wanted to show us. There were a lot of little kids that elicited plenty of ***“aww”s from the audience***, and a few older kids who took their talents and made them into works of art. The variety in what they showed us was astounding. Every single one of them brought out what was unique about them. 
>
> **I’ll be honest--when the spotlight was set on us, my heart was hammering.** But it turned out to be a little like dancing. A little rocky at the beginning, as you’re getting used to your stage, but then, when you’re familiar with the floor, you get more comfortable just winging it. You may make a mistake, or forget a portion of the choreography, but there are always other people helping you to your feet, guiding you through it all. And sometimes you find yourself in that position, sharing someone’s errors and making it into something lighthearted, an incident to laugh at with your family over dinner. 
>
> **While it was fun and lively, it was also a nice learning experience.** Not just about hosting an event, or keeping conversation going, but having fun while doing so. That, I think, is one of the great things about homeschooling. 

<br>

Congratulations, Laya, you did very well as an emcee! Thank you Arrows & Quivers Homeschool, Teacher Grace Gaston-Dousel, for hosting this momentous occasion! Good Job to all those who had shared their talents and skills!  ***A Very Happy National Homeschool Day to all homeschoolers in Mindanao! Maayong Gabii kaninyong tanan!***
