---
title: Frequently Asked Questions
layout: long
---

# Frequently Asked Questions

HAPI recommends two excellent resources for answering every question found here and likely every other question you'll have about homeschooling. Much of the content of our answers was taken from these two books, both from seasoned Filipino homeschooling families.

<div class="flex justify-around">
    <figure class="pr-5 text-center flex-1">
        <img alt="iHomeschool" src="/img/ihomeschool.jpg">
        <figcaption>
            <a href="https://ihomeschool.ph/products/ihomeschool-book"><em>iHomeschool</em></a> by Nove&#8209;Ann&nbsp;Tan
        </figcaptioon>
    </figure>
    <figure class="text-center flex-1">
        <img alt="Why You Should Homeschool" src="/img/wysh.jpg">
        <figcaption>
            <a href="https://learningplus.ph/products/why-you-should-homeschool"><em>Why You Should Homeschool</em></a> <br>by Edric and Joy Mendoza
        </figcaption>
    </figure>
</div>

## What is homeschooling?
---
Homeschooling is parent-led, home-based education. Because it can be customized, the learning process in homeschooling takes on many forms. These may include employing a tutor or online resource and sharing the job with other homeschooling parents. For some students, especially older, more independent learners, studying is usually self-directed, although parents are still involved. The similiar factor is that the parents are “steering” the whole process.

### DepEd's Definition

The Philippine Department of Education expounds on the definition and guidelines surrounding homeschooling in *[Policy Guidelines on the K to 12 Basic Education Program](https://www.deped.gov.ph/wp-content/uploads/2019/08/DO_s2019_021.pdf)*, Order No. 021, s. 2019.

<details>
<summary>Annex 3 contains the following provisions:</summary>
<blockquote>
9\. Home schooling provides learners with access to formal education while staying in an out-of-school environment. Authorized parents, guardians, or tutors take the place of the teachers as learning facilitators. While learners are expected to meet the learning standards of the K to 12 Basic Education Curriculum, the learning facilitators are given flexibility in learning delivery, scheduling, assessment, and curation of learning resources. The program aims to cater to learners who may
require home schooling because of their unique circumstances, such as illness, frequent travelling, special education needs, and other similar contexts. Moreover, the program allows parents and guardians to maximize their involvement in their children's education as a matter of parenting philosophy.

10\. Parents or guardians who opt to enroll their children in a home school program should do so through a public school, or through a private school that had been given a permit to offer a home school program. These schools be in charge of ensuring the following:  
a. that a parent or guardian is assigned as a learning facilitator;  
b. that the learner is registered through the Learner Information System
(LIS);  
c. that learning materials and resources are available for the learner; and  
d. the School Form 9 and other pertinent school records are issued to  
homeschoolers.

11\. Alternatively, learners may be enrolled through home school providers,
which are learning centers or institutions that provide home schooling. To facilitate
the registration of learners through the Learner Information System (LIS), these
providers may either find a DepEd accredited partner school, or they may coordinate
with the SDO directly.
</blockquote>
</details>

## Who should homeschool?

---
We firmly believe that “every family who can homeschool, should homeschool”. Ideally, this would mean a family who can afford to have at least one parent supervising the daily instruction.

## Is homeschooling legal in the Philippines? 

---

Yes!  
  
The 1987 Constitution in its Declaration of Principles and State Policies acknowledges the "**rearing of the youth as the natural and primary right and duty of parents** which shall receive the support of the Government" (*Philippine Constitution* Art. II Sec.12).

> The State shall protect and promote the right of all citizens to quality education at all levels
> and shall encourage, among others, non-formal and informal systems as well as self-learning and independent study program (Art. XIV Sec 2).

  
One of HAPI's biggest accomplishments was homeschooling's recognition through the Philippine National Homeschool Day on March 3, with the help of Senator Francis N. Pangilinan who authored the senate resolution. The first ever National Homeschool Day was celebrated in 2017.
  
Read more about the establishment of the Philippine National Homeschool Day [here](http://www.senate.gov.ph/press_release/2017/0303_pangilinan1.asp) and [here](https://www.senate.gov.ph/lisdata/2544621965!.pdf).

## Why choose homeschooling?

---
Homeschooling creates an environment where customized and personalized learning can take place. This gives children the opportunity to excel academically. More importantly, the amount of time spent together as a family allows the parents to be the primary influence in the lives of their children. As a result, parents can more effectively pass on the values and character traits they want their children to learn. Furthermore, because homeschooled children socialize vertically, i.e.across ages, rather than horizontally, they become less peer-dependent. The flexibility of homeschooling also provides children with more time to play, to be outdoors, to pursue interests, hobbies, and sports which they can excel in.

<!-- In a poll conducted in the Facebook group of the Homeschoolers of the Philippines, homeschooling's top ten benefits are:
  

01. Bonding with family  
02. Flexibility  
03. Discipleship  
04. Room to explore  
05. More time together
06. Having the world as classroom  
07. Own pacing to learn  
08. Potential to develop self esteem  
09. Positive character  
10. Both parents and children learn   -->

  

## Important HAPI terms to know

---
**Homeschool Provider** – an institution that offers a homeschool program. These may or may not be accredited by the Philippines’ Department of Education. For inclusion into the Department of Education system, homeschoolers are required to be enrolled in a Department of Education accredited homeschool provider.  
**Compliant Curriculum** – a curriculum which meets the learning goals of the Department of Education for the corresponding year level  
**Homeschool Program** - a system of compliant curricula and mechanisms to support the parent and the homeschooled student.  
**HAPI** – the Homeschool Association of the Philippine Islands (www.hapi.ph)  
**LRN** – Learner Reference Number  
As a requirement for Basic Education, every child needs a unique Learner Reference Number (LRN). Basing on DO 22, s.2012 – Adoption of the Unique Learner Reference Number.
  
The LRN is a permanent twelve (12)-digit number which the student shall keep while completing the basic education program, regardless of transfer to another school or learning center in the public or private sector, and promotion/moving up to the secondary level.
  
In order to facilitate the tracking of pupils/students/learners and their performance, a Unique Learner Reference Number (LRN) will be issued to all public school pupils and students and Alternative Learning System (ALS) learners based on the School Year.
  
The LRN shall be incorporated in all documents, forms, examinations, surveys, and databases which refer to a pupil, student or learner.   
  
For those enrolled with Department of Education accredited homeschool providers, the LRN is issued by the providers. For those who are not enrolled with such providers (independent homeschoolers with or without accreditation), a LRN may be obtained through a PEPT (Philippine Educational Placement Test).
  
The PEPT however was designed for re-entry of students into a the Department of Education’s system (through mainstream schooling or enrollment with Deped accredited homeschool providers.) For more information on the PEPT, please check [this post](http://www.filipinohomeschooler.com/pept-what-is-it-and-who-is-it- for/).
​

## Are there requirements to become a homeschool teacher?

---
All parent teachers enrolled in a Department of Education Accredited homeschool provider must be college graduates. Parent-teachers who do not hold college degrees may however still homeschool independently and have their student-children re-enter the Department of Education system via the PEPT.

